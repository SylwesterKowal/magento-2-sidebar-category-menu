<?php


namespace Kowal\LeftSidebarCategoryMenu\Block;

/**
 * Class SidebarCategoryMenu
 *
 * @package Kowal\LeftSidebarCategoryMenu\Block
 */
class SidebarCategoryMenu extends \Magento\Catalog\Block\Navigation //\Magento\Framework\View\Element\Template
{

    /**
     * @var \Magento\Catalog\Model\CategoryFactory
     */
    protected $_categoryInstance;
    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManagerInterface;

    protected $_recursionLevel;

    /**
     * SidebarCategoryMenu constructor.
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Magento\Catalog\Model\CategoryFactory $categoryFactory
     * @param \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory
     * @param \Magento\Catalog\Model\Layer\Resolver $layerResolver
     * @param \Magento\Framework\App\Http\Context $httpContext
     * @param \Magento\Catalog\Helper\Category $catalogCategory
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Catalog\Model\Indexer\Category\Flat\State $flatState
     * @param array $data
     * @param \Magento\Store\Model\StoreManagerInterface $StoreManagerInterface
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Catalog\Model\CategoryFactory $categoryFactory,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
        \Magento\Catalog\Model\Layer\Resolver $layerResolver,
        \Magento\Framework\App\Http\Context $httpContext,
        \Magento\Catalog\Helper\Category $catalogCategory,
        \Magento\Framework\Registry $registry,
        \Magento\Catalog\Model\Indexer\Category\Flat\State $flatState,
        array $data = [],

        \Magento\Store\Model\StoreManagerInterface $StoreManagerInterface
    )
    {
        parent::__construct($context, $categoryFactory, $productCollectionFactory, $layerResolver, $httpContext, $catalogCategory, $registry, $flatState, $data);

        $this->_productCollectionFactory = $productCollectionFactory;
        $this->_catalogLayer = $layerResolver->get();
        $this->httpContext = $httpContext;
        $this->_catalogCategory = $catalogCategory;
        $this->_registry = $registry;
        $this->flatState = $flatState;
        $this->storeManagerInterface = $StoreManagerInterface;
        $this->_categoryInstance = $categoryFactory->create();

        $this->_recursionLevel = max(
            0,
            (int)$context->getScopeConfig()->getValue(
                'catalog/navigation/max_depth',
                \Magento\Store\Model\ScopeInterface::SCOPE_STORE
            )
        );
    }

    /**
     * @return string
     */
    public function sidebarCategoryMenu()
    {
        $desktopHtml = array();
        $mobileHtml = array();
        $rootCatId = $this->getRootCategoryId();
        $catListTop = $this->getChildExt($rootCatId);

        $last = count($catListTop);
        $dropdownIds = [];
        foreach ($catListTop as $catTop) {
            $idTop = $catTop->getEntityId();
            $urlTop = '<a class="level-top" href="' . $catTop->getUrl() . '">' . $this->getThumbnail($catTop) . '<span>' . __($catTop->getName()) . $this->getCatLabel($catTop) . '</span><span class="boder-menu"></span></a>';

            $classTop = $this->isCategoryActive($idTop) ? ' active' : '';
            $isDropdown = ' dropdown';
            // drawMainMenu
            $options = '';
            if ($this->_recursionLevel == 1) {
                $menu = array('desktop' => '', 'mobile' => '');
            } else {
                if ($isDropdown) {
                    $classTop .= $isDropdown;
                    $childHtml = $this->getTreeCategoriesExt($idTop); // include magic_label
                    // $childHtml = $this->getTreeCategoriesExtra($idTop); // include magic_label and Maximal Depth
                    $menu = array('desktop' => $childHtml, 'mobile' => $childHtml);
                }
            }

            if ($menu['desktop']) $classTop .= ' hasChild parent';

            $desktopHtml[$idTop] = '<li class="level0 cat ' . $classTop . '"' . $options . '>' . $urlTop . $menu['desktop'] . '</li>';
            $mobileHtml[$idTop] = '<li class="level0 cat ' . $classTop . '">' . $urlTop . $menu['mobile'] . '</li>';
        }
        $menu['desktop'] = $desktopHtml;
        $menu['mobile'] = implode("\n", $mobileHtml);
        $this->setData('mainMenu', $menu);
        return $menu;
    }

    public function  getTreeCategoriesExt($parentId) // include Magic_Label
    {
        $categories = $this->_categoryInstance->getCollection()
            ->addAttributeToSelect(array('name','magic_label','url_path'))
            ->addAttributeToFilter('include_in_menu', 1)
            ->addAttributeToFilter('parent_id', $parentId)
            ->addIsActiveFilter()
            ->addAttributeToSort('position', 'asc');
        $html = '';
        foreach($categories as $category)
        {
            $level = $category->getLevel();
            $childHtml = ( $this->_recursionLevel == 0 || ($level -1 < $this->_recursionLevel) ) ? $this->getTreeCategoriesExt($category->getId()) : '';
            $childClass = $childHtml ? ' hasChild parent' : '';
            $childClass .= $this->isCategoryActive($category->getId()) ? ' active' : '';
            $html .= '<li class="level' . ($level -2) . $childClass . '"><a href="' . $category->getUrl() . '"><span>' . $category->getName() . $this->getCatLabel($category) . "</span></a>\n" . $childHtml . '</li>';
        }
        if($html) $html = '<ul class="level'.($level -3).' submenu">' .$html. '</ul>';
        return $html;
    }

    private function getChildExt($parentId)
    {
        $collection = $this->_categoryInstance->getCollection()
            ->addAttributeToSelect(array('entity_id', 'name', 'magic_label', 'url_path'))
            ->addAttributeToFilter('parent_id', $parentId)
            ->addAttributeToFilter('include_in_menu', 1)
            ->addIsActiveFilter()
            ->addAttributeToSort('position', 'asc'); //->addOrderField('name');
        return $collection;
    }

    private function getRootCategoryId()
    {
        return $this->storeManagerInterface->getStore()->getRootCategoryId();
    }

    private function getStoreId()
    {
        return $this->storeManagerInterface->getStore()->getId();
    }

    public function isCategoryActive($catId)
    {
        return $this->getCurrentCategory() ? in_array($catId, $this->getCurrentCategory()->getPathIds()) : false;
    }
}

